/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   generalv2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/18 02:28:29 by gabettin          #+#    #+#             */
/*   Updated: 2019/09/18 02:32:17 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_h.h"

static int	get_iptu(t_printf *ptf, t_opt *opt, t_long *lo)
{
	if (opt->o & l)
		lo->u = (unsigned long)va_arg(*ptf->va, unsigned long);
	else if (opt->o & ll)
		lo->u = (unsigned long long)va_arg(*ptf->va, unsigned long long);
	else if (opt->o & h)
		lo->u = (unsigned short)va_arg(*ptf->va, unsigned int);
	else if (opt->o & hh)
		lo->u = (unsigned char)va_arg(*ptf->va, unsigned int);
	else
		lo->u = (unsigned int)va_arg(*ptf->va, unsigned int);
	return (0);
}

int			get_iptv2(t_printf *ptf, t_opt *opt, t_long *lo, int u)
{
	if (u == 1)
		return (get_iptu(ptf, opt, lo));
	if (opt->o & l)
		lo->s = (long)va_arg(*ptf->va, long);
	else if (opt->o & ll)
		lo->s = (long long)va_arg(*ptf->va, long long);
	else if (opt->o & h)
		lo->s = (short)va_arg(*ptf->va, int);
	else if (opt->o & hh)
		lo->s = (char)va_arg(*ptf->va, int);
	else
		lo->s = (int)va_arg(*ptf->va, int);
	return (0);
}
