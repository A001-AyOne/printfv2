/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chared_tag.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/23 00:45:23 by gabettin          #+#    #+#             */
/*   Updated: 2019/08/30 01:38:09 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_h.h"

void		simple_width(t_opt *opt, int p[2], char *s, t_printf *ptf)
{
	int i;

	i = (p[0] != -1 && p[0] < p[1] ? p[0] : p[1]);
	if (!(opt->o & minus))
		while (opt->w-- > i)
			put_buffer(' ', ptf);
	put_buffers(s, i, ptf);
	while (opt->w-- > i)
		put_buffer(' ', ptf);
}

int			chared_tag(t_printf *ptf, t_opt *opt)
{
	char	l;
	int		len[3];
	char	*str;

	l = ptf->format[ptf->i];
	len[2] = (l == 's' ? 1 : 0);
	str = get_iptc(ptf, opt, (char*)(len + 2));
	if (l == 's')
	{
		if (str == NULL)
			str = "(null)";
		len[1] = ft_strlen(str);
		len[0] = opt->p;
		simple_width(opt, len, str, ptf);
		return (1);
	}
	if (l == 'c')
	{
		len[0] = 1;
		len[1] = 1;
		simple_width(opt, len, (char*)(len + 2), ptf);
		return (1);
	}
	return (0);
}
