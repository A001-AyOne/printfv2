/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   options.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/16 03:14:06 by gabettin          #+#    #+#             */
/*   Updated: 2019/09/10 04:57:50 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_h.h"

/*
**here is the format or the flags:
**[#] -> [[-+]0[*]1234567890] -> [.[*1234567890]] -> [hhllL] -> (%diouxXcspf)
**
**
**
** check_zero " [+-# 0]
*/

static void	check_zero(t_opt *opt, t_printf *ptf, int *i)
{
	while (ft_charisin(ptf->format[*i], "+-# 0") > -1)
	{
		if (ptf->format[*i] == '+')
			opt->o += (opt->o & plus) ? 0 : plus;
		else if (ptf->format[*i] == '-')
			opt->o += (opt->o & minus) ? 0 : minus;
		else if (ptf->format[*i] == '#')
			opt->o += (opt->o & hash) ? 0 : hash;
		else if (ptf->format[*i] == ' ')
			opt->o += (opt->o & space) ? 0 : space;
		else if (ptf->format[*i] == '0')
			opt->o += (opt->o & zero) ? 0 : zero;
		else
			break ;
		(*i)++;
	}
}

/*
** check_one : [[*][0123456789]] -> [.[*0123456789]]
*/

static void	check_one(t_opt *opt, t_printf *ptf, int *i)
{
	while (1)
	{
		if (ptf->format[*i] == '*' && ++opt->bw
			&& (*i)++ > 0 && ((opt->w = va_arg(*ptf->va, int)) || !opt->w))
			continue ;
		else if (!(ptf->format[*i] >= '1' && ptf->format[*i] <= '9'))
			break ;
		opt->w = ft_atoi(ptf->format + *i);
		while (ptf->format[*i] <= '9' && ptf->format[*i] >= '0')
			(*i)++;
		break ;
	}
	if (ptf->format[*i] == '.' && (*i)++ > 0)
	{
		if (ptf->format[*i] == '*' && (*i)++ > 0)
			return (void)(++opt->bp + (opt->p = va_arg(*ptf->va, int)));
		if (!(ptf->format[*i] >= '1' && ptf->format[*i] <= '9'))
		{
			*i += ptf->format[*i] == '0' ? 1 : 0;
			return ((void)(opt->p = 0));
		}
		opt->p = ft_atoi(ptf->format + *i);
		while (ptf->format[*i] <= '9' && ptf->format[*i] >= '0')
			(*i)++;
	}
}

/*
** check_two : [hhllL]
*/

static void	check_two(t_opt *opt, t_printf *ptf, int *i)
{
	if (ptf->format[*i] == 'h' && (opt->o += h) > 0)
		(*i)++;
	if (ptf->format[*i] == 'h' && ((opt->o += hh - h)) > 0)
		(*i)++;
	if (ptf->format[*i] == 'l' && (opt->o += l) > 0)
		(*i)++;
	if (ptf->format[*i] == 'l' && ((opt->o += ll - l)) > 0)
		(*i)++;
	if (ptf->format[*i] == 'L' && (opt->o += L) > 0)
		(*i)++;
	opt->o += opt->bw && opt->w < 0 ? minus : 0;
	opt->w = opt->bw ? ft_abs(opt->w) : opt->w;
	opt->p = opt->p < 0 ? -1 : opt->p;
}

t_opt		check_format(t_printf *ptf)
{
	char	step;
	t_opt	opt;
	int		i;

	i = ++ptf->i;
	(step = 0) == 0 ? ft_bzero(&opt, sizeof(t_opt)) : 0;
	opt.p = -1;
	opt.w = -1;
	opt.bw = 0;
	opt.bp = 0;
	check_zero(&opt, ptf, &i);
	check_one(&opt, ptf, &i);
	check_two(&opt, ptf, &i);
	ptf->i = i;
	opt.l = ptf->format[i];
	return (opt);
}
