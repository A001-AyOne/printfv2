/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   output.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/19 15:58:41 by gabettin          #+#    #+#             */
/*   Updated: 2019/08/30 01:42:06 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_h.h"

int			clean_buffer(t_printf *ptf)
{
	write(1, ptf->buf, ptf->len);
	ptf->tlen += ptf->len;
	ft_bzero(ptf->buf, FTPRINTF_BUFFER);
	ptf->len = 0;
	return (ptf->tlen);
}

int			put_buffer(char c, t_printf *ptf)
{
	ptf->buf[ptf->len++] = c;
	if (ptf->len < FTPRINTF_BUFFER)
		return (clean_buffer(ptf));
	return (0);
}

int			put_buffers(char *cs, int len, t_printf *ptf)
{
	int rtn;
	int i;

	if (len < 0)
		len = ft_strlen(cs);
	i = 0;
	while (i < len)
		rtn = put_buffer(cs[i++], ptf);
	return (rtn);
}
