/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   based_tag.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/19 16:00:58 by gabettin          #+#    #+#             */
/*   Updated: 2019/09/18 02:25:32 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_h.h"

static int		smart_swap(char **r, char **full)
{
	if (*r == *full)
		return (1);
	free(*r);
	*r = *full;
	return (1);
}

#include <stdio.h>

static char		*clc(t_opt *opt, char *r, char rs[5][1024])
{
	char	*full;

	if (opt->p != -1 && !rs[4][0])
	{
		free(r);
		r = ft_strdup("");
	}
	full = (rs[3][0] == '0' ? ft_strjoin(rs[3], r) : r);
	smart_swap(&r, &full);
	full = (rs[2][0] ? ft_strjoin(rs[2], r) : r);
	smart_swap(&r, &full);
	full = ((rs[1][0] && rs[4][0] == 1)
			|| opt->l == 'p' ? ft_strjoin(rs[1], r) : r);
	smart_swap(&r, &full);
	full = (rs[0][0] ? ft_strjoin(rs[0], r) : r);
	smart_swap(&r, &full);
	full = (rs[3][0] == ' ' && !(opt->o & minus) ? ft_strjoin(rs[3], r) : r);
	smart_swap(&r, &full);
	full = (rs[3][0] == ' ' && opt->o & minus ? ft_strjoin(r, rs[3]) : r);
	return (smart_swap(&r, &full) ? full : full);
}

static void		ext(t_opt *opt, int len, char rs[5][1024])
{
	int i;
	int len_;

	i = 0;
	printf("|%d->", len);
	len_ = len;
	len_ -= (rs[0][0] ? 1 : 0 || opt->o & plus + space)
		+ (opt->l == 'o' && opt->o & hash && opt->p == 0 ? 1 : 0)
		+ (opt->l == 'p' && rs[4][0] == 1 ? 2 : 0)
		+ (ft_charisin(opt->l, "xX") > -1 && opt->o & hash
			&& rs[4][0] == 1 && opt->p > -1 ? 2 : 0);
	printf("%d vs %d->", len_, opt->p);
	if (opt->p == 0 && opt->l == 'o' && opt->o & hash && rs[4][0] == 0)
		opt->p++;
	while (opt->p - i > len_)
		rs[2][i++] = '0';
	printf("{%s}", rs[2]);
	i = 0;
	len_ = (opt->p > len_ ? opt->p : len_) + (opt->l == 'p' && rs[4][0] == 0)
		+ (opt->l != 'p' && opt->p == -1 && rs[4][0] == 0)
		+ (opt->l == 'p' && rs[4][0] == 1 ? 2 : 0);
	if (rs[0][0] || opt->o & plus + space)
		len_ += 1 + (rs[4][0] == 0);
	printf("%d vs %d '%c'", len_, opt->w, (opt->o & zero && !(opt->o & minus) && opt->p < len ? '0' : ' '));
	while (opt->w - i > len_)
		rs[3][i++] = (opt->o & zero && !(opt->o & minus)
			&& opt->p < len_ ? '0' : ' ');
	printf("{%s}|", rs[3]);
}

/*
**       0     1     2          3      4
** rs = [sign][hash][precision][width][lastC == '0']
*/

static char		*clc_it(t_opt *opt, char *r, int u)
{
	char	rs[5][1024];
	int		len;

	len = 0;
	while (len < 5)
		ft_bzero(rs[len++], 1024);
	len = ft_strlen(r);
	len = (r[0] == '0' && (opt->p == 0 || opt->o & hash) ? 0 : len);
	rs[4][0] = (len == 0 ? 0 : 1);
	if ((u == -1 || u == 1))
	{
		u == -1 && rs[4][0] == 1 ? rs[0][0] = '-' + 0 * len++ : 0;
		if (u == 1 && opt->o & space + plus && ++len)
			rs[0][0] = (opt->o & space + plus && opt->o & plus ? '+' : ' ');
	}
	if ((opt->o & hash && ft_charisin(opt->l, "oxX")) || opt->l == 'p')
	{
		rs[1][0] = '0' + 0 * len++;
		if (opt->l != 'o')
			rs[1][1] = (opt->l == 'p' || opt->l == 'x' ? 'x' : 'X') + 0 * len++;
	}
	ext(opt, len, rs);
	return (clc(opt, r, rs));
}

int				based_tag(t_printf *ptf, t_opt *opt)
{
	long long			s;
	unsigned long long	u;
	char				*r[4];

	r[3] = (ft_charisin(opt->l, "pbouxX") > -1 ? NULL + 1 : NULL);
	u = (int)r[3];
	opt->o += opt->l == 'p' && (opt->o & l) == 0 ? l : 0;
	if (get_ipt(ptf, opt, &s, &u) == 0 && opt->l == 'b')
		r[0] = ft_unbrbase(u, "01");
	else if (opt->l == 'o')
		r[0] = ft_unbrbase(u, "01234567");
	else if (opt->l == 'u' || opt->l == 'd' || opt->l == 'i')
		r[0] = opt->l != 'u'
			? ft_nbrbase(s, "0123456789") : ft_unbrbase(u, "0123456789");
	else if (opt->l == 'x' || opt->l == 'p')
		r[0] = ft_unbrbase(u, "0123456789abcdef");
	else if (opt->l == 'X')
		r[0] = ft_unbrbase(u, "0123456789ABCDEF");
	else
		return (-1);
	r[2] = (r[0][0] == '-' ? ft_strdup(r[0] + 1) : ft_strdup(r[0]));
	free(r[0]);
	r[1] = clc_it(opt, r[2], (r[3] != NULL ? 0 : 1) * (s >= 0 ? 1 : -1));
	put_buffers(r[1], -1, ptf) * 0 == 0 ? free(r[1]) : 0;
	return (1);
}
