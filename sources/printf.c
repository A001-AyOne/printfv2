/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 17:28:40 by gabettin          #+#    #+#             */
/*   Updated: 2019/09/18 06:19:34 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_h.h"

static t_func	op(char l, int (*f)(struct s_printf *ptf, t_opt *opt))
{
	t_func rslt;

	rslt.l = l;
	rslt.f = f;
	return (rslt);
}

static void		init_printf(t_printf *ptf, va_list *va)
{
	ptf->i = 0;
	ptf->error = 0;
	ptf->va = va;
	ptf->len = 0;
	ptf->tlen = 0;
	ft_bzero(ptf->buf, FTPRINTF_BUFFER);
	ft_bzero(ptf->funcs, sizeof(t_func) * 20);
	ptf->funcs[0] = op('p', generate_base);
	ptf->funcs[1] = op('d', generate_base);
	ptf->funcs[2] = op('i', generate_base);
	ptf->funcs[3] = op('o', generate_base);
	ptf->funcs[4] = op('u', generate_base);
	ptf->funcs[5] = op('x', generate_base);
	ptf->funcs[6] = op('X', generate_base);
	ptf->funcs[7] = op('b', generate_base);
	ptf->funcs[8] = op('%', pourcent);
	ptf->funcs[9] = op('s', chared_tag);
	ptf->funcs[10] = op('c', chared_tag);
}

static int		ext(t_printf *ptf)
{
	int		i;
	char	l;
	t_opt	opt;

	i = 0;
	opt = check_format(ptf);
	l = ptf->format[ptf->i];
	while (ptf->funcs[i].l != 0)
	{
		if (ptf->funcs[i].l == l)
			return (ptf->funcs[i].f(ptf, &opt));
		i++;
	}
	return (-1);
}

int				ft_printf(const char *format, ...)
{
	va_list		va;
	t_printf	ptf;

	(void)put_buffers;
	init_printf(&ptf, &va);
	ptf.format = format;
	va_start(va, format);
	while (ptf.format[ptf.i] != 0)
	{
		if (ptf.error == 0 && ptf.format[ptf.i] == '%')
		{
			if (ext(&ptf) < 0)
				ptf.error = 1;
		}
		else
			put_buffer(ptf.format[ptf.i], &ptf);
		if (ptf.format[ptf.i] != '\0')
			ptf.i++;
	}
	va_end(va);
	return (clean_buffer(&ptf));
}
