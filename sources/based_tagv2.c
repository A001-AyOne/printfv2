/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   based_tagv2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/18 01:59:39 by gabettin          #+#    #+#             */
/*   Updated: 2019/10/04 06:57:17 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_h.h"
#include <stdio.h>

int			debug_fd;

static int	smart_swap(t_ipt *ipt)
{
	if (ipt->buff == ipt->work)
		return (1);
	free(ipt->work);
	ipt->work = ipt->buff;
	return (1);
}

static void	*next(t_ipt *ipt)
{
	ipt->sc[0] = (ipt->sign && ipt->l.s < 0 ? '-' : 0);
	if (ipt->sign && !ipt->sc[0])
		ipt->sc[0] = (ipt->opt->o & plus ? '+' : ' ');
	ipt->sc[1] = '\0';
	dprintf(debug_fd, "sign char: %c\n", ipt->sc[0]);
	dprintf(debug_fd, "put {%s} at {%s} : %d\n", ipt->w, ipt->work, ipt->wc == '0');
	ipt->buff = (ipt->wc == '0' ? ft_strjoin(ipt->w, ipt->work) : ipt->work);
	smart_swap(ipt);
	dprintf(debug_fd, "put {%s} at {%s} : %d\n", ipt->p, ipt->work, 1);
	ipt->buff = ft_strjoin(ipt->p, ipt->work);
	smart_swap(ipt);
	dprintf(debug_fd, "put {%s} at {%s} : %d\n", ipt->h, ipt->work, !ipt->empty || ipt->pl > 0);
	ipt->buff = (ft_charisin(ipt->opt->l, "op") > -1 || !ipt->empty
			|| ipt->pl > 0 ? ft_strjoin(ipt->h, ipt->work) : ipt->work);
	smart_swap(ipt);
	dprintf(debug_fd, "put {%s} at {%s} : %d\n", ipt->sc, ipt->work, ipt->sign);
	ipt->buff = (ipt->sign ? ft_strjoin(ipt->sc, ipt->work) : ipt->work);
	smart_swap(ipt);
	dprintf(debug_fd, "put {%s} at {%s} : %d\n", ipt->w, ipt->work, ipt->wc == ' ' && !(ipt->opt->o & minus));
	ipt->buff = (ipt->wc == ' ' && !(ipt->opt->o & minus)
			? ft_strjoin(ipt->w, ipt->work) : ipt->work);
	smart_swap(ipt);
	dprintf(debug_fd, "put {%s} at {%s} : %d\n", ipt->work, ipt->w, ipt->wc == ' ' && ipt->opt->o & minus);
	ipt->buff = (ipt->wc == ' ' && ipt->opt->o & minus
			? ft_strjoin(ipt->work, ipt->w) : ipt->work);
	smart_swap(ipt);
	dprintf(debug_fd, "result : \"%s\"\n", ipt->work);
	put_buffers(ipt->work, ft_strlen(ipt->work), ipt->ptf);
	free(ipt->work);
	free(ipt->w);
	free(ipt->p);
	return (NULL + 1);
}

static void	*clc_it(t_ipt *ipt)
{
	int i;

	ipt->hl = (ft_charisin(ipt->opt->l, "xXpb") > -1
		&& ipt->opt->o & hash && ipt->opt->p != 0 ? 2 : 0) + (ipt->opt->o & hash && ipt->opt->l == 'o' ? 1 : 0);
	dprintf(debug_fd, "hash len : %-10d", ipt->hl);
	ipt->pl = ipt->opt->p; //- (ipt->opt->p > 1 && ipt->opt->l == 'o' && ipt->opt->o & hash ? 1 : 0);
	dprintf(debug_fd, "prec len : %d\n", ipt->pl);
	ipt->wl = ipt->opt->w - ipt->sign;
	dprintf(debug_fd, "width len: %-10d", ipt->wl);
	ipt->wc = (ipt->pl == -1 && (ipt->opt->o & zero + minus) == zero ? '0' : ' ');
	dprintf(debug_fd, "w char   : %c (%d)\n", ipt->wc, ipt->opt->o & (zero + minus));
	ipt->len = (ipt->empty && ipt->pl > -1 ? 0 : ipt->len + ipt->hl);
	ipt->len = !(ipt->opt->l == 'p' && ipt->empty) ? ipt->len : ipt->hl;
	dprintf(debug_fd, "new len  : %-10d", ipt->len);
	if ((ipt->p = malloc((ipt->pl > 0 ? ipt->pl + 1 : 1))) == NULL)
		return (NULL);
	if ((ipt->w = malloc((ipt->wl > 0 ? ipt->wl + 1: 1))) == NULL)
		return (NULL);
	ft_bzero(ipt->p, (ipt->pl > 0 ? ipt->pl + 1 : 1));
	ft_bzero(ipt->w, (ipt->wl > 0 ? ipt->wl + 1 : 1));
	if (ipt->hl)
		if ((ipt->h[0] = '0') && ft_charisin(ipt->opt->l, "xXbp") > -1)
			ipt->h[1] = (ipt->opt->l == 'p' ? 'x' : ipt->opt->l);
	dprintf(debug_fd, "raw hash : %s\n", ipt->h);
	i = ipt->wl - (ipt->len - ipt->hl > ipt->pl ? ipt->len : ipt->pl + ipt->hl);
	dprintf(debug_fd, "||%d||\n", i);
	while (--i >= 0)
		ipt->w[i] = ipt->wc;
	i = (ipt->pl - (ipt->len - ipt->hl)) - (ipt->opt->l == 'o' && ipt->empty ? ipt->hl : 0);
	while (--i >= 0)
		ipt->p[i] = '0';
	dprintf(debug_fd, "====next====\n");
	return (next(ipt));
}

static void tagging(t_opt *opt)
{
	if (opt->l == 'p')
	{
		opt->o -= opt->o & (hash + l + ll);
		opt->o += hash + ll;
	}
	if (opt->l == 'o')
	{
		if (opt->o & hash)
			opt->p = opt->p != -1 ? opt->p - 1 : 0;
	}
}

int		generate_base(t_printf *ptf, t_opt *opt)
{
	t_ipt	ipt;

	debug_fd = open("debug.txt", O_RDWR | O_CREAT);

	ipt.ptf = ptf;
	if ((ipt.opt = opt) && ft_charisin(opt->l, "diu") > -1)
		ipt.base = (opt->l != 'u' ? "s0123456789" : "u0123456789");
	else if (ft_charisin(opt->l, "pxX") > -1)
		ipt.base = (opt->l != 'X' ? "u0123456789abcdef" : "u0123456789ABCDEF");
	else if (opt->l == 'b')
		ipt.base = "u01";
	else if (opt->l == 'o')
		ipt.base = "u01234567";
	else
		return (0);
	ipt.unsi = (ipt.base[0] == 'u' ? 1 : 0);
	dprintf(debug_fd, "unsigned : %-10d", ipt.unsi);
	tagging(opt);
	get_iptv2(ptf, opt, &ipt.l, ipt.unsi);
	dprintf(debug_fd, "input    : %lld\n", ipt.l.s);
	ipt.buff = (ipt.unsi ? ft_unbrbase(ipt.l.u, ipt.base + 1)
							: ft_nbrbase(ipt.l.s, ipt.base + 1));
	dprintf(debug_fd, "raw input: %-10s", ipt.buff);
	ipt.sign = (!ipt.unsi && (ipt.buff[0] == '-' || opt->o & space + plus));
	dprintf(debug_fd, "sign     : %d\n", ipt.sign);
	ipt.len = ft_strlen(ipt.buff) - (ipt.buff[0] == '-');
	dprintf(debug_fd, "len      : %-10d", ipt.len);
	ipt.empty = (ipt.len == 1 && ipt.buff[ipt.buff[0] == '-'] == '0');
	dprintf(debug_fd, "is empty : %d\n", ipt.empty);
	ipt.work = ft_strdup((ipt.empty && opt->p > -1 ? "" : ipt.buff + (ipt.buff[0] == '-')));
	dprintf(debug_fd, "no sign  : %-10s", ipt.work);
	ft_safefree(((void**)&ipt.buff));
	ft_bzero(ipt.h, 3);
	dprintf(debug_fd, "\n====clc_it====\n");
	(clc_it(&ipt) == NULL ? exit(3) : 0);
	close(debug_fd);
	return (1);
}
