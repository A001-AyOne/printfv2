/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   basic_tag.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/20 03:02:15 by gabettin          #+#    #+#             */
/*   Updated: 2019/08/30 01:36:25 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_h.h"

int			pourcent(t_printf *ptf, t_opt *opt)
{
	int i;

	if (!(opt->o & minus) || opt->o & zero)
		while (opt->w-- > (opt->p > 1 ? opt->p : 1))
			put_buffer((opt->o & zero) ? '0' : ' ', ptf);
	i = 0;
	while (opt->p - i++ > 1)
		put_buffer('0', ptf);
	put_buffer('%', ptf);
	while (opt->w-- > (opt->p > 1 ? opt->p : 1))
		put_buffer(' ', ptf);
	return (1);
}
