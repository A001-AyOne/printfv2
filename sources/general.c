/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   general.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/19 16:03:39 by gabettin          #+#    #+#             */
/*   Updated: 2019/09/20 03:58:14 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_h.h"

static int	get_iptu(t_printf *ptf, t_opt *opt,
										unsigned long long *u)
{
	if (opt->o & l)
		*u = (unsigned long)va_arg(*ptf->va, unsigned long);
	else if (opt->o & ll)
		*u = (unsigned long long)va_arg(*ptf->va, unsigned long long);
	else if (opt->o & h)
		*u = (unsigned short)va_arg(*ptf->va, unsigned int);
	else if (opt->o & hh)
		*u = (unsigned char)va_arg(*ptf->va, unsigned int);
	else
		*u = (unsigned int)va_arg(*ptf->va, unsigned int);
	return (0);
}

int			get_ipt(t_printf *ptf, t_opt *opt, long long *s,
					unsigned long long *u)
{
	if (*u == 1)
		return ((*s = 0) + get_iptu(ptf, opt, u));
	if (opt->o & l)
		*s = (long)va_arg(*ptf->va, long);
	else if (opt->o & ll)
		*s = (long long)va_arg(*ptf->va, long long);
	else if (opt->o & h)
		*s = (short)va_arg(*ptf->va, int);
	else if (opt->o & hh)
		*s = (char)va_arg(*ptf->va, int);
	else
		*s = (int)va_arg(*ptf->va, int);
	return (*u = 0);
}

char		*get_iptc(t_printf *ptf, t_opt *opt, char *len)
{
	(void)opt;
	if (*len == 0)
		return (NULL + 0 * (*len = (char)va_arg(*ptf->va, int)));
	if (*len == 1)
		return (va_arg(*ptf->va, char*));
	return (NULL);
}
