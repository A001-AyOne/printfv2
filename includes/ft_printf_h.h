/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_h.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 14:44:18 by gabettin          #+#    #+#             */
/*   Updated: 2019/09/20 03:51:39 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
**       0     1     2          3      4
** based_tag.c
** based_tagv2.c
** basic_tag.c
*/

#ifndef FT_PRINTF_H_H
# define FT_PRINTF_H_H
# include <sys/types.h>
# include <stdarg.h>
# include <unistd.h>
# include "libft.h"
# define FTPRINTF_BUFFER 16

struct s_printf;

typedef union	u_long
{
	unsigned long long u;
 	long long s;
}				t_long;

typedef enum	e_mask
{
	l = 1,
	ll = 2,
	h = 4,
	hh = 8,
	hash = 16,
	zero = 32,
	plus = 64,
	minus = 128,
	space = 256,
	wstar = 512,
	pstar = 1024,
	L = 2048,
}				t_mask;

typedef struct	s_opt
{
	unsigned int	o;
	int				w;
	int				p;
	int				bw : 1;
	int				bp : 1;
	char			l;
}				t_opt;

typedef struct	s_func{
	int			(*f)(struct s_printf *ptf, t_opt *opt);
	char		l;

}				t_func;

typedef struct	s_printf
{
	va_list		*va;
	t_func		funcs[20];
	const char	*format;
	int			i;
	long		tlen;
	int			len;
	char		buf[FTPRINTF_BUFFER];
	int			error;
}				t_printf;

typedef struct s_ipt
{
	t_printf		*ptf;
	t_opt			*opt;
	char			*base;
	char			*work;
	char			*buff;
	int				len;
	char			*w;
	char			wc;
	int				wl;
	char			*p;
	int				pl;
	char			h[3];
	int				hl;
	unsigned int	empty : 1;
	unsigned int	unsi : 1;
	unsigned int	sign : 1;
	char			sc[2];
	t_long			l;

}				t_ipt;

t_opt			check_format(t_printf *ptf);
int				clean_buffer(t_printf *ptf);
int				put_buffer(char c, t_printf *ptf);
int				put_buffers(char *cs, int len, t_printf *ptf);
int				get_ipt(t_printf *ptf, t_opt *opt, long long *s,
						unsigned long long *u);
char			*get_iptc(t_printf *ptf, t_opt *opt, char *len);
int				based_tag(t_printf *ptf, t_opt *opt);
int				chared_tag(t_printf *ptf, t_opt *opt);
int				pourcent(t_printf *ptf, t_opt *opt);


int			get_iptv2(t_printf *ptf, t_opt *opt, t_long *lo, int u);
int			generate_base(t_printf *ptf, t_opt *opt);
#endif
