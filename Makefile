.PHONY: all clean fclean re ft_printf lib

NAME		= libftprintf.a

GCC			= gcc

#sources_files
SRC_FILES	=	printf.c options.c output.c general.c based_tag.c basic_tag.c chared_tag.c \
				generalv2.c based_tagv2.c
#sources_folder
SRC_DIR		= sources
OBJ_DIR		= objects
#includes_folder
INC_DIR		= includes
SOURCES		= $(addprefix $(SRC_DIR)/, $(SRC_FILES))
OBJECTS		= $(addprefix $(OBJ_DIR)/,$(SRC_FILES:.c=.o))

FLAGS		= -Wall -Wextra -Werror
DFLAGS		= $(FLAGS) -g3
RFLAGS		= $(FLAGS) -O3
INCLUDES	= -Ilibft/includes -I/usr/local/include -Iincludes
LIBS		= libft/libft.a

all: $(NAME)
	@rm -rf debug.txt
	@touch debug.txt
	gcc main.c libftprintf.a -Iincludes -Ilibft/includes -o a.out
	gcc main2.c libftprintf.a -Iincludes -Ilibft/includes -o b.out

lib: Makefile
	@make all -C libft -j ARGS="$(DFLAGS)" BUILD=Debug
	@make all -C libft -j ARGS="$(RFLAGS)" BUILD=Release
	@make local_lib -C libft BUILD=Release

$(NAME): dir lib $(OBJECTS) $(INC_DIR)/* Makefile
	@cp $(LIBS) $(NAME)
	@ar rc $(NAME) $(OBJECTS)

dir:
	@mkdir -p objects

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c $(INC_DIR)/* Makefile
	@$(GCC) $(FLAGS) -c $(INCLUDES) $< -o $@

clean:
	@make clean --no-print-directory -C libft
	@rm -rf $(OBJECTS)

fclean: clean
	@rm -rf $(NAME)
	@rm -rf ft_printf
	@make fclean --no-print-directory -C libft BUILD=Debug

re:fclean all ft_printf
