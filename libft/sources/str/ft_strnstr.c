/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/06 19:00:15 by gabettin          #+#    #+#             */
/*   Updated: 2019/03/07 22:41:36 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	const char	*old_needle;
	const char	*old_haystack;
	size_t		old_len;

	if (*needle == '\0' || needle == haystack)
		return ((char*)haystack);
	else if (*haystack == '\0')
		return (NULL);
	old_needle = needle;
	old_haystack = haystack;
	while (*haystack != '\0' && len-- > 0)
	{
		needle = old_needle;
		old_len = len + 1;
		while (*(haystack++) == *(needle++) && old_len-- > 0)
			if (*needle == '\0')
				return ((char*)(haystack + (old_haystack - haystack)));
		old_haystack++;
		haystack = old_haystack;
	}
	return (NULL);
}
