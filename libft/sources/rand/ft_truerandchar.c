/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_truerandchar.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/17 15:49:16 by gabettin          #+#    #+#             */
/*   Updated: 2019/04/17 15:58:14 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	ft_truerandchar(char min, char max)
{
	if (max < min)
		return (0);
	if (max == min)
		return (max);
	return ((rand() % (max - min) + min));
}
