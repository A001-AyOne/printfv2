/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nbrbase.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gabettin <gabettin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/18 13:53:40 by gabettin          #+#    #+#             */
/*   Updated: 2019/09/09 00:26:30 by gabettin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	rec(unsigned long nbr, char *base, int *vars, char *result)
{
	if (nbr >= (unsigned long)vars[0])
		rec(nbr / vars[0], base, vars, result);
	result[vars[1]++] = base[nbr % vars[0]];
}

char		*ft_nbrbase(long nbr, char *base)
{
	unsigned long	nbr_;
	char			buffer[50];
	int				vars[2];

	ft_bzero(buffer, 50);
	if (nbr < 0)
		buffer[0] = '-';
	nbr_ = (nbr < 0) ? -nbr : nbr;
	if ((vars[0] = ft_strlen(base)) <= 1)
		return (NULL);
	vars[1] = (nbr < 0) ? 1 : 0;
	rec(nbr_, base, vars, buffer);
	return (ft_strdup(buffer));
}

char		*ft_unbrbase(unsigned long nbr, char *base)
{
	char			buffer[50];
	int				vars[2];

	ft_bzero(buffer, 50);
	if ((vars[0] = ft_strlen(base)) <= 1)
		return (NULL);
	vars[1] = 0;
	rec(nbr, base, vars, buffer);
	return (ft_strdup(buffer));
}
